<h1>Questions and Exercises</h1>

Here it will be all questions and exercises that will get you more ready for a job interview or to ace up your skills. It will be mostly about<strong>JS,CSS, HTML</strong> in the front-end, and <strong>Node.js</strong> in back end. This also with be added for the [Codepedia project](https://github.com/mugas/codepedia).

<h4>HTML</h4>

<p><strong>Q1:</strong>What is the purpose of the <em>alt</em> attribute on images?</p>

<p>Provides alternative information for an image if a user cannot view it.<strong>Decorative images should have an empty <em>alt</em> attribute. Web crawlers use this tag to understand image content.</strong>

<p><strong>Q2:</strong>What is the purpose of cache busting and how can you achieve it</p>

<p>Browsers store files in a cache so they don't need to be dowloaded again when the website reloads or between pages.But if the website has been changed the user's cache still refers to the old files. <em>Cache busting</em> is the process of forcing the browser to dowload new files. This is done by naming the file something different to the old file</p>

```html
src="js/script.js" =>src="js/script.js?v=2"
```

<p><strong>Q3:</strong>Briefly describe the correct usage of the following HTML5 semantic elements: <em>header, article,section,footer</em>?</p>

<p>
header - introductory and navigational information about a section of the page.

article - House self-contained composition. Individual blog posts or new stories.

section - holding content that shares a common informational theme or purpose.

footer - Additional information about the section.

<p><strong>Q4:</strong>What are <em>defer</em> and <em>async</em> on a <em>script</em>tag</p>?

In neither the script is dowloaded and executed synchronously and will halt parsling of the document until it has finished executing.

The `defer` downloads the document while the document is still parsing but waits untill the document has finished parsing before executing it.

The `async` dowloads the scipt during parsing the document but will pause the parser to execute the script it has fully finished parsing. `async`scripts will not necessarily execute in order.

They must only be used if the script has a `src`attribute.

Puting the `defer` in the head allows the browser to dowload the script while the page is still parsing, beeing a better option than placing the script before the end of the body.

<p><strong>Q5:</strong>What is the DOM?</p>

It's the <strong> Document Object Model</strong> and it's a cross plataform API that treats HTML and XML documents as a tree structure consisting of nodes.
These nodes are objects that can be manipulated and any visible changes are reflected live in the document.
The DOM was designed to be independent of any particular programming language, making the structural representation of the document, available from a API.
The DOM is contructed progressively in the browser as a page loads, which is why scripts are placed at the bottom of a page.

<p><strong>Q6:</strong> What is the difference between HTML and React event handling?</p>

In HTML, the event name should be in lowercase, but in React follows the camelCase convention.

```javascript
//HTML

<button onclick>...

//React

<button onClick>
```

<p><strong>Q7:</strong> Where and why is the <strong>rel="noopener"</strong> attribute used?</p>

It's an attribute used in <a> elements. Prevents page from having a `window.opener` property, which would otherwise point to the page from where the link was opened and would allow the page opened from the hyperlink to manipulate the page where the hyperlink is.

<p><strong>Q8:</strong> What is HTML5 Web Storage?Explain<strong>localStorage</strong> and <strong>sessionStorage</strong></p>

Data store `localStorage`is permanent till the web app deletes or the user ask the browser to delete it.
The `sessionStorage` has the same lifetime as the top-level window or browser in which the data got stored.When the `tab`is closed, any data stored is deleted.

<h4>CSS</h4>

<p><strong>Q1:</strong>What is CSS BEM?</p>

Is a naming convention for `CSS classes` in order to keep `CSS` more maintainable by defining namespaces to solve scoping issues. It stands for <strong> Block Element Modifier</strong>.
A block is a standalone component that is reusable across projects and acts as a "namespace" for subcomponents(Elements). Modifiers are used as flags when a Block or Element is in certain state.

```html
<nav class="navbar">
  <a href="/" class="navbar__link navbar__link--active"></a>
  <a href="/" class="navbar__link"></a>
</nav>
```

In this case `navbar`is th Block, `navbar__link` is an Element that makes no sense outside of the navbarcomponent, and `navbar__link--active` is a Modifier that indicates a different state for the `navbar__link`Element.

<p><strong>Q2:</strong>What are the advantages of using CSS preprocessors?</p>

They allow to write more maintainable and scalable CSS. It has some disadvantages like setup, re-compilation, etc.

Nowadays with `css variables` and addittional tools make `CSS`more like a real programming language.

<p><strong>Q3:</strong>Can you name the four types of @media properties</p>

<ul>
    <li>all</li>
    <li>print</li>
    <li>screen</li>
    <li>speech</li>
</ul>

<p><strong>Q3:</strong>Describe the layout of the CSS Box Model and briefly describe each component</p>

<strong>Content:</strong> The inner-most part of the box with content, such as text, image, etc. The dimensions of it are `content-box width` and `content-box height`.

<strong>Padding</strong>The transparent area surrounding the content. The dimensions are `padding-box width` and `padding box-height`

<strong>Border</strong>The area surroding the border and content.

<strong>Margin</strong>The transparent outer-most layer that surrounds the border. It separates the element from other elements in the DOM.

<p><strong>Q4</strong> What is the difference between <em>em</em> and <em>rem</em> units?</p>

Both are based on the `font-sized` CSS property.The different is where they inherit their values from.

`em`inherits their value from the `font-size`of the parent element while `rem`inherits their value from the `font-size`of the root(html) element.

Most browsers have the `font-size`of the root elements set to `16px`

<p><strong>Q5</strong>What are the advantages of using CSS sprites and how they are utilized?</p>

`CSS`sprites combine multiple images into one image, limiting the number of HTTP requests a browser has to make.
To utilize a `spritesheet`in `CSS, one would use certain properties, such as`background-image, `background-position`to ultimately alter the `background`of an element.

<p><strong>Q6</strong>What is the difference between '+' and '~'?</p>

The `~` selects all elements that are siblings of a specified element while the `+` selects the elements that are adjacent(right after) a specified element.

<p><strong>Q7</strong>Can you describe how CSS specificity works?</p>

Each rule is assigned a matrix of values, which correspond to the following from highest to lowest specificity:

<ul>
    <li>Inline rules</li>
    <li>Number of id selectors</li>
    <li>Number of class, pseudo class and attribute selectors</li>
    <li>Number of tags and pseudo - elements selectors</li>
</ul>

When two selectors are compared, the comparison is made on a per column basis.
In cases of equal specificity between multiple rules, the rules that comes last is deemed more specific

<p><strong>Q8</strong>What is a focus ring? What is the correct solution to handle them?</p>

Is a visible outline given to focusable elements such as buttons and anchor tags. Generally it appears as a blue outline around the element to indicate it is currently focused.
The best solution is a upcoming pseudo selector `:focus-visible`.

<h4>Javascript</h4>

<p><strong>Q1</strong>What is the differente between <em>==</em> and <em>===</em>?</p>

Triple equals `===`checks for strick equality, so both the `type`and `value`must be the same. Double equals `==` performs `type coercion`so that both operands are of the same type and then applies strict comparison.

Whenever possible, use triple equals to test equality. `Type coercion`means the values are converted into the same type.

<p><strong>Q2</strong>What is the difference between Element and Component?</p>

An `Element` is a plain object describing what you want to appear in terms of the `DOM`nodes or other components. Elements can contain other elements in their props.

A `component` can be declared in several different ways. `Components` take props as an input and return an element tree as the output.

<p><strong>Q3</strong>What is the difference between the postfix <em>i++</em> and prefix <em>++i</em> increment operators</p>

Both increment the value by 1. The difference is what they evaluate to.

The `postfix`evalutes to the value <strong>before</strong>it was incremented.

```javascript
let i = 0;
i++; //0
//i === 1
```

The `prefix`evaluates to the value <strong>after</strong> it was incremented

```javascript
let i = 0;
++i; //1
//i === 1
```

<p><strong>Q4</strong>In which states can a <strong>Promise</strong>be?</p>

<ul>
    <li><strong>Pending</strong> - Initial state, nor fullfilled nor rejected</li>
    <li><strong>fullfilled</strong> - Operation completed successfully</li>
    <li><strong>rejected</strong> - Operation failed</li>
</ul>

<p><strong>Q4</strong>What is a stateful component?</p>

It's a component whose behavior depends on its state. Stateful components are always class components and have a state that is initialized in the constructor.

```javascript
class App extends Component {
  constructor(props) {
    super(props);
    this.state = { count: 0 };
  }
  render() {
    //...
  }
}
```

<p><strong>Q5</strong>Create a function batches that returns the maximum number of whole batches that can be cooked from a recipe.</p>

```javascript
/**
It accepts two objects as arguments: the first object is the recipe
for the food, while the second object is the available ingredients.
Each ingredient's value is number representing how many units there are.

`batches(recipe, available)`
*/

// 0 batches can be made
batches(
  { milk: 100, butter: 50, flour: 5 },
  { milk: 132, butter: 48, flour: 51 }
);
batches(
  { milk: 100, flour: 4, sugar: 10, butter: 5 },
  { milk: 1288, flour: 9, sugar: 95 }
);

// 1 batch can be made
batches(
  { milk: 100, butter: 50, cheese: 10 },
  { milk: 198, butter: 52, cheese: 10 }
);

// 2 batches can be made
batches(
  { milk: 2, sugar: 40, butter: 20 },
  { milk: 5, sugar: 120, butter: 500 }
);
```

We must have all ingredients of the recipe available, and in quantities that are more than or equal to the number of units required. If one of ingredients is not available or lower than we need, we cannot make a single batch.

Use `object.keys()`to return the ingredients of the recipe as an array, then use `Array.prototype.map()`to map each ingredient to the ratio of available units to the amount required by the recipe.
If one of the ingredients by the recipe is not available the ratio will be `NaN`.
Use the`...` spread operator to feed the array of all the ingredients ratios into `Math.min()`to determine the lowest ratio. Passing this result into `Math.floor()`.

```javascript
const batches = (recipe, available) =>
  Math.floor(
    Math.min(...Object.keys(recipe).map(k => available[k] / recipe[k] || 0))
  );
```

<p><strong>Q6</strong>What is a callback?Give a example</p>

`Callbacks` are functions passed as an argument to another function to be executed once an event has ocurred or a certain task is complete, often used in asynchronous code.
`Callbacks`functions are invoked later by a piece of code but can be declared on initialization without being invoked.

As an example, event listeners are asynchrounous callbacks that are only executed when a specif event occurs.

```javascript
function onClick() {
  console.log("The user clicked on the page");
}
document.addEventListener("click", onClick);
```

However, callbacks can be synchronous. The following `map`function takes a callback function that is invoked synchronously for each iteration of the loop(array element)

```javascript
const map = (arr, callback) => {
  const result = [];
  for (let i = 0; i < arr.length; i++) {
    result.push(callback(arr[i], i));
  }
  return result;
};
map([1, 2, 3, 4, 5], n => n * 2); // [2,4,6,8,10]
```

<p><strong>Q7</strong>What is the difference between an expression and a statement in Javascript?</p>

An `expression`produces a value and a `statement`performs an action.

If you can print it or assign it to a variable, it's an `expression`. If you can't it's a `statement`

<h4>Statements</h4>

```javascript
let x = 0;
function declaration() {
  if (true) {
  }
}

var x;
if (y >= 0) {
  x = y;
} else {
  x = -y;
}
```

<h4>Expression</h4>

```javascript
5 + 5; // =>10
lastCharachter("input"); //=>"t"

true === true; // =>true
```

<p><strong>Q8</strong>What is event delegation and why it is usefull?</p>

`Event delegation`is a technique of delegating events to a single common ancestor. Events bubble up the DOM tree by executing any handlers progressively on each ancestor element up to the root that may be listening to it.

Instead of

```javascript
document.querySelectorAll("button).forEach(button=>{
  button.addEventlistener("click",handleButtonClick)
})

//Event delegation involves using a condition to ensure the child target matches our desired element

document.addEventListener("click", e=>{
  if(e.target.closest("button)){
    handleButtonClick()
  }
})
```

<p><strong>Q9</strong>What is the difference between the array methods map() and forEach()</p>

Both methods iterate through the elements of an array. `map()` maps each element to a new element by invoking the `callback` function on each element and returning a new array. `forEach()` is used when causing a side effect on each iteration, whereas `map()`. It doesn't creates a new array. Good to mutate elements while iterating over them.

<p><strong>109</strong>How does hoisting work in Javascript</p>

Is a Javascript mechanism where variable and function declarations are put into memory during the compile phase. This means that no matter where functions and variables are declared, they are moved to the top of their scope regardless of whether their scope is global or local.

For example:

```javascript
console.log(hoist);
var hoist = "value";
```

Is the same as :

```javascript
var hoist;
console.log(hoist);
hoist = "value";
```

So hoist would be `undefined`.

And hoisting also allows for function declaration:

```javascript
myFunction()

function myFunction(){
  console.log("hello)
}
//It works

myFunction()
var function myFunction(){
 console.log("hello")
}
//It will give error
```

//Create a function that masks a string of characters with # except for the last four (4) characters.
